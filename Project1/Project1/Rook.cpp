#include "Rook.h"

/*
the c'tor
*/
Rook::Rook(char type, int x, int y, const int color) : Tools(type, x, y, color)
{
}

/*
the d'tor
*/
Rook::~Rook()
{
}

/*
input: the new x,y of the new place that we want to check if we can move to there and the board
output: true or false if we can move the rook
the function check if we can move the rook to a new place
*/
bool Rook::checkMove(int new_x, int new_y,const std::vector<std::vector<Tools*>>& _board)
{
	int i = 0;
	if (this->_x_place != new_x && this->getY() != new_y)
	{
		return false;
	}
	if (this->_x_place != new_x)
	{
		if (this->_x_place < new_x)
		{
			for (i = this->_x_place + 1; i < new_x; i++)
			{
				if (_board[i][this->_y_place] != nullptr)
				{
					return false;
				}
			}
		}
		else
		{
			for (i = this->_x_place - 1; i > new_x; i--)
			{
				if (_board[i][this->_y_place] != nullptr)
				{
					 return false;
				}
			}
		}
	}
	else if (this->_y_place != new_y)
	{
		if (this->_y_place < new_y)
		{
			for (i = this->_y_place + 1; i < new_y; i++)
			{
				if (_board[this->_x_place][i] != nullptr)
				{
					return false;
				}
			}
		}
		else
		{
			for (i = this->_y_place - 1; i > new_y; i--)
			{
				if (_board[this->_x_place][i] != nullptr)
				{
					return false;
				}
			}
		}
	}
	if (_board[new_x][new_y] != nullptr)
	{
		return this->can_eat(new_x, new_y, _board);
	}
	return true;
}

