#pragma once
#define PLAY true
#define NOT_PLAY false

/*
the class of the players
*/

class Players
{
public:
	//c'tor
	Players();
	//d'tor
	~Players();
	//functions
	bool get_white()const;//get if us white's turn
	bool is_black()const;//get if is the black turn
	void set_players(); //change the turn of the playres

private:
	bool _whitePlayer; 
	bool _blackPlayer;

};

