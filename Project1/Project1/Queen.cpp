#include "Queen.h"

/*
the c'tor
*/
Queen::Queen(const char type, const int x, const int y, const int color):Tools(type, x, y, color)
{
}

/*
the d'tor
*/
Queen::~Queen()
{
}

/*
input: the new x,y of the new place that we want to check if we can move to there and the board
output: true or false if we can move the queen
the function check if we can move the queen to a new place
*/
bool Queen::checkMove(int new_x, int new_y, const std::vector<std::vector<Tools*>>& _board)
{
	return (checkStraight(new_x, new_y, _board) || checkSlant(new_x, new_y, _board));
}

/*
input: the new x,y of the new place that we want to check if we can move to there and the board
output: true or false if we can move the queen on the straight
the function check if we can move the queen to a new place on a straight
*/
bool Queen::checkStraight(int new_x, int new_y, const std::vector<std::vector<Tools*>>& _board)
{
	int i = 0;
	if (this->_x_place != new_x && this->getY() != new_y)
	{
		std::cerr << "invalid place";
		return false;
	}
	if (this->_x_place != new_x)
	{
		if (this->_x_place < new_x)
		{
			for (i = this->_x_place + 1; i < new_x; i++)
			{
				if (_board[i][this->_y_place] != nullptr)
				{
					return false;
				}
			}
		}
		else
		{
			for (i = this->_x_place - 1; i > new_x; i--)
			{
				if (_board[i][this->_y_place] != nullptr)
				{
					return false;
				}
			}
		}
	}
	else if (this->_y_place != new_y)
	{
		if (this->_y_place < new_y)
		{
			for (i = this->_y_place + 1; i < new_y; i++)
			{
				if (_board[this->_x_place][i] != nullptr)
				{
					return false;
				}
			}
		}
		else
		{
			for (i = this->_y_place - 1; i > new_y; i--)
			{
				if (_board[this->_x_place][i] != nullptr)
				{
					return false;
				}
			}
		}
	}
	if (_board[new_x][new_y] != nullptr)
	{
		return this->can_eat(new_x, new_y, _board);
	}
	return true;
}

/*
input: the new x,y of the new place that we want to check if we can move to there and the board
output: true or false if we can move the queen on the slant
the function check if we can move the queen to a new place on a slant
*/
bool Queen::checkSlant(int new_x, int new_y, const std::vector<std::vector<Tools*>>& _board)
{
	if ((std::abs(this->_x_place - new_x)) == (std::abs(this->_y_place - new_y)))
	{
		int minX = std::min(this->_x_place, new_x);
		int YofMinX = 0;

		if (minX == this->_x_place)
		{
			YofMinX = this->_y_place;
		}
		else
		{
			YofMinX = new_y;
		}

		if (YofMinX != std::min(new_y, this->_y_place))
		{
			int j = std::max(new_y, this->_y_place);
			for (int i = minX; i <= std::max(this->_x_place, new_x); i++, j--)
			{
				if (_board[i][j] != nullptr)
				{
					if (_board[this->_x_place][this->_y_place] != _board[i][j] && _board[new_x][new_y] != _board[i][j])
					{
						return false;
					}
				}
			}
		}
		else
		{
			int j = YofMinX;
			for (int i = minX; i <= std::max(this->_x_place, new_x); i++, j++)
			{
				if (_board[i][j] != nullptr)
				{
					if (_board[this->_x_place][this->_y_place] != _board[i][j] && _board[new_x][new_y] != _board[i][j])
					{
						return false;
					}
				}
			}
		}
		if (_board[new_x][new_y] != nullptr)
		{
			return this->can_eat(new_x, new_y, _board);
		}
		return true;
	}
	return false;
}