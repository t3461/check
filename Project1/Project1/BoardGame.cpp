#include "BoardGame.h"

/*
the c'tor
the c'tor is take the string of the start board and initialize the board according to the string
*/
BoardGame::BoardGame()
{ 
	this->_size = SIZE;
    _str_board = "rnbkqbnrpppppppp################################PPPPPPPPRNBKQBNR0";
    int k = 0;

    for (int i = 0; i < SIZE; i++)
    { 
        std::vector<Tools*> newColumn;
        for (int j = 0; j < SIZE; j++)
        {
            Tools* t = nullptr;
            switch (this->_str_board[k])
            {
            case 'R':
            {
                t = new Rook('R', i, j, WHITE);
                break;
            }
            case 'N':
            {
                t = new Knight('N', i, j, WHITE);
                break;
            }
            case 'B':
            {
                t = new Bishop('B', i, j, WHITE);
                break;
            }
            case 'K':
            {
                t = new King('K', i, j, WHITE);
                break;
            }
            case 'Q':
            {
                t = new Queen('Q', i, j, WHITE);
                break;
            }
            case 'P':
            {
                t = new Pawn('P', i, j, WHITE);
                break;
            }
            case '#':
            {
                t = nullptr;
                break;
            }
            case 'p':
            {
                t = new Pawn('p', i, j, BLACK);
                break;
            }
            case 'r':
            {
                t = new Rook('r', i, j, BLACK);
                break;
            }
            case 'n':
            {
                t = new Knight('n', i, j, BLACK);
                break;
            }
            case 'b':
            {
                t = new Bishop('b', i, j, BLACK);
                break;
            }
            case 'k':
            {
                t = new King('k', i, j, BLACK);
                break;
            }
            case 'q':
            {
                t = new Queen('q', i, j, BLACK);
                break;
            }
            }
            k++;
            newColumn.push_back(t);
        }
        this->_board.push_back(newColumn);
    }
} 

/*
the d'tor
*/
BoardGame::~BoardGame()
{
}

/*
input:none
oputput:none
the function print the board 
*/
void BoardGame::printBoard()
{
    std::cout << this->_str_board << std::endl;
    for (int i = 0; i < this->_board.size(); i++)
    {
        for (int j = 0; j < this->_board[i].size(); j++)
        {
            if (this->_board[i][j] == nullptr)
            {
                std::cout << "# ";
            }
            else
            {
                std::cout << this->_board[i][j]->getType() << " ";
            }
        }
        std::cout << "\n";
    }
}

/*
input:none
oputput:the boatrd
the function return the board
*/
std::vector<std::vector<Tools*>>& BoardGame::getBoard()
{
    return this->_board;
}

/*
input:which player is his turn this turn
oputput:none
the function update the str according to the player who is turn now and the tools
*/
void BoardGame::update_str_board(int who_play)
{
    this->_str_board = "";
    for (int i = 0; i < SIZE; i++)
    {
        for (int j = 0; j <SIZE; j++)
        {
            if (this->_board[i][j] == nullptr)
            {
                this->_str_board += "#";
            }
            else
            {
                this->_str_board += this->_board[i][j]->getType();
            }
        }
    } 
    this->_str_board += (char)who_play + '0';
}