#pragma once
#include <iostream>
#include "Tools.h"

/*
* the queen class that heir from tools
*/

class Queen : public Tools
{
public:
	//c'tor
	Queen(const char type, const int x, const int y, const int color);
	//d'tor
	~Queen();
	virtual bool checkMove(int new_x, int new_y,const std::vector<std::vector<Tools*>>&  _board);//check if the nove is valid
	bool checkStraight(int new_x, int new_y, const std::vector<std::vector<Tools*>>& _board);//check if is staright nove
	bool checkSlant(int new_x, int new_y,const std::vector<std::vector<Tools*>>& _board);//check if is slant move
};