#pragma once

#include <iostream>
#include <algorithm>
#include "Tools.h"

/*
* the bishop class that heir from tools
*/

class Bishop : public Tools
{
public:
	Bishop(const char type, const int x, const int y, const int color);//c'tor
	~Bishop();//d'tor
	virtual bool checkMove(int new_x, int new_y, const std::vector<std::vector<Tools*>>& _board);//check if the nove is valid
};


