#include "Knight.h"

/*
the c'tor
*/
Knight::Knight(const char type, const int x, const int y, const int color) :Tools(type, x, y, color)
{
}

/*
the d'tor
*/
Knight::~Knight()
{
}

/*
input: the new x,y of the new place that we want to check if we can move to there and the board
output: true or false if we can move the knight
the function check if we can move the knight to a new place
*/
bool Knight::checkMove(int new_x, int new_y, const std::vector<std::vector<Tools*>>& _board)
{
	if ((std::abs(this->getX() - new_x) == 1 && std::abs(this->getY() - new_y) == 2) || (std::abs(this->getX() - new_x) == 2 && std::abs(this->getY() - new_y) == 1))
	{
		if (_board[new_x][new_y] != nullptr)
		{
			return this->can_eat(new_x, new_y, _board);
		}
		return true;
	}
	return false;
}
