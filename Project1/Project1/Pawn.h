#pragma once
#include "Tools.h"
#include <cmath>
#include <iostream>
#include "Tools.h"

/*
* the pawn class that heir from tools
*/

class Pawn : public Tools {

public:
	//c'tor
	Pawn(const char type, const int x, const int y, const int color, const bool didFirstMove = false);
	//d'tor
	~Pawn();
	//functions
	void setDidFirstMove(const bool didFirstMove);
	bool getDidFirstMove();
	virtual bool checkMove(int new_x, int new_y, const std::vector<std::vector<Tools*>>& _board);//check if the nove is valid

private:
	bool _didFirstMove;
};