#include "Tools.h"

/*
the c'tor 
input:the type od the tool, the x of the place of the tool, the y of the place of the tool, the color of the player that his turn now
oputput:none
*/
Tools::Tools(const char type, const int x, const int y, const int color)
{
	this->_type = type;
	this->_color = color;
	this->_x_place = x;
	this->_y_place = y;
}

/*
the d'tor
*/
Tools::~Tools()
{
}

/*
input: the color of the tool
output: none
the function set the color of the tool
*/
void Tools::setColor(int color)
{
	this->_color = color;
}

/*
input: the type of the tool
output: none
the function set the type of the tool
*/
void Tools::setType(char type)
{
	this->_type = type;
}

/*
input: the x place of the tool
output: none
the function set the x place of the tool
*/
void Tools::setX(int x)
{
	this->_x_place = x;
}

/*
input: the y place of the tool
output: none
the function set the y place of the tool
*/
void Tools::setY(int y)
{
	this->_y_place = y;
}

/*
input: none
output: the type of the tool
the function return the type of the tool
*/
char Tools::getType()
{
	return this->_type;
}

/*
input: none
output: the x place of the tool
the function return the x place of the tool
*/
int Tools::getX()
{
	return this->_x_place;
}

/*
input: none
output: the color of the tool
the function return the color of the tool
*/
int Tools::getColor()
{
	return this->_color;
}

/*
input: none
output: the y place of the tool
the function return the y place of the tool
*/
int Tools::getY()
{
	return this->_y_place;
}

/*
input: the new x,y place on the board that we move the tool to there
output: none
the function move a tool from place to new place
*/
void Tools::move(int new_x, int new_y, std::vector<std::vector<Tools*>>& _board)
{
	if (_board[new_x][new_y] != nullptr)
	{
		this->eat(new_x, new_y, _board);
	}
	_board[new_x][new_y] = _board[this->_x_place][this->_y_place]; 
	_board[this->_x_place][this->_y_place] = nullptr; 
	this->setX(new_x);
	this->setY(new_y);
}

/*
input: the new x,y place on the board that we eat the tool there
output: none
the function eat with the player the player that is on the new place
*/
void Tools::eat(int new_x, int new_y, std::vector<std::vector<Tools*>>& _board)
{
	if (_board[this->_x_place][this->_y_place]->can_eat(new_x, new_y, _board))
	{
		delete _board[new_x][new_y];
	}
}

/*
input: the x,y place on the board that we want to check if this place is taken or not
output: true or false if the place is taken or not
the function check if the place that we got is taken or not
*/
bool Tools::isPlaceTaken(int x, int y, std::vector<std::vector<Tools*>> &_board)
{
	if (_board[x][y] != nullptr)
	{
		return true;
	}
	return false;
}

/*
input: the new board game
output: if ther a chess on the board
the function check if there a chess on the board
*/
bool Tools::is_chess(const std::vector<std::vector<Tools*>>& _board)
{
	int x_rival_king = 0; 
	int y_rival_king = 0; 

	for (int i = 0; i < SIZE; i++)
	{
		for (int j = 0; j < SIZE; j++)
		{
			if (_board[i][j] != nullptr)
			{
				if (_board[this->_x_place][this->_y_place]->getColor() == WHITE && _board[i][j]->getType() == 'k')
				{
					x_rival_king = i;
					y_rival_king = j;
					break;
				}
				if (_board[this->_x_place][this->_y_place]->getColor() == BLACK && _board[i][j]->getType() == 'K')
				{
					x_rival_king = i;
					y_rival_king = j;
					break;
				}
			}
		}
	}
	return this->checkMove(x_rival_king, y_rival_king, _board);
}

/*
input: the new x,y place on the board that we want to check if we can eat the tool there
output: if we can eat or not
the function check if we can eat the player that is on the new place
*/
bool Tools::can_eat(int new_x, int new_y, const std::vector<std::vector<Tools*>>& _board)
{
	return this->_color != _board[new_x][new_y]->getColor();
}

/*
input: the new x,y place on the board that we want to check if the move will cause to chess 
output: true or false if the move will cause chess
the function check if the move will cause chess
*/
bool Tools::moveCausesChess(int new_x, int new_y, std::vector<std::vector<Tools*>>& _board)//����� ���� ��� 
{
	

		if (_board[new_x][new_y] != nullptr)
		{
			Tools* temp = _board[this->getX()][this->getY()];
			_board[this->getX()][this->getY()] = nullptr; 
			for (int i = 0; i < SIZE; i++)
			{
				for (int j = 0; j < SIZE; j++)
				{
					if (_board[i][j] != nullptr && _board[i][j]->getColor() != temp->getColor()&&(new_x != i && new_y != j) &&_board[i][j]->is_chess(_board))
					{
						_board[this->getX()][this->getY()] = temp;
						return false;
					}
				}
			}
			_board[this->getX()][this->getY()] = temp;
			return true; 
		}
		else
		{
			_board[new_x][new_y] = _board[this->getX()][this->getY()];
		    _board[this->getX()][this->getY()] = nullptr;
			for (int i = 0; i < SIZE; i++)
			{
				for (int j = 0; j < SIZE; j++)
				{
					if (_board[i][j] != nullptr && _board[i][j]->getColor() != _board[new_x][new_y]->getColor() &&(new_x != i && new_y != j) &&_board[i][j]->is_chess(_board) )
					{
						_board[this->getX()][this->getY()] = _board[new_x][new_y];
						_board[new_x][new_y] = nullptr; 
						return false;
					}
				}
			}
			_board[this->getX()][this->getY()] = _board[new_x][new_y];
			_board[new_x][new_y] = nullptr;
			return true;
		}
}

