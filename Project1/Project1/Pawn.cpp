#include "Pawn.h"

/*
the c'tor
*/
Pawn::Pawn(const char type, const int x, const int y, const int color, const bool didFirstMove): Tools(type, x, y, color)
{
	this->_didFirstMove = didFirstMove;
}

/*
the d'tor
*/
Pawn::~Pawn()
{
}

/*
input: the new bool that say if the pawn did his first move
output: none
the function check if we can move the bishop to a new place
*/
void Pawn::setDidFirstMove(const bool didFirstMove)
{
	this->_didFirstMove = didFirstMove;
}

/*
input: none
output: return true or false if the pawn did his first move
the function check if the pawn did his first move
*/
bool Pawn::getDidFirstMove()
{
	return this->_didFirstMove;
}

/*
input: the new x,y of the new place that we want to check if we can move to there and the board
output: true or false if we can move the pawn
the function check if we can move the pawn to a new place
*/
bool Pawn::checkMove(int new_x, int new_y, const std::vector<std::vector<Tools*>>&  _board)
{
	if ((this->_y_place == new_y) && (((this->_x_place + 1) == new_x) || ((this->_x_place + 2) == new_x)) && this->getColor() == BLACK)
	{
		if (((this->_x_place + 1) == new_x && this->_didFirstMove) || !this->_didFirstMove)
		{
			if (_board[new_x][new_y] != nullptr)
			{
				return false;
			}
			if (!this->_didFirstMove)
			{
				this->setDidFirstMove(true);
			}
			return true;
		}	
		return false; 
	}

	else if ((this->_y_place == new_y) && (((this->_x_place - 1) == new_x) || ((this->_x_place - 2) == new_x)) && this->getColor() == WHITE)
	{
		if (((this->_x_place - 1) == new_x && this->_didFirstMove) || !this->_didFirstMove)
		{
			if (_board[new_x][new_y] != nullptr)
			{
				return false;
			}
			if (!this->_didFirstMove)
			{
				this->setDidFirstMove(true);
			}
			return true;
		}
		return false;
	}

	else if(((this->_x_place + 1 == new_x && this->getColor() == BLACK) || (this->_x_place - 1 == new_x && this->getColor() == WHITE))  && std::abs(this->_y_place - new_y) == 1)
	{
		if (_board[new_x][new_y] != nullptr)
		{
			if (this->can_eat(new_x, new_y, _board) && !this->_didFirstMove)
			{
				this->setDidFirstMove(true);
			}
			return this->can_eat(new_x, new_y, _board); 
		}
		return false;
	}
	return false;
}