#pragma once
#include <iostream>
#include "Tools.h"
#define DID_FIRST_MOVE true
#define DIDNT_DO_FIRST_MOVE false

/*
* the king class that heir from tools
*/

class King : public Tools
{
public:
	//c'tor
	King(const char type, const int x, const int y, const int color);
	//d'tor
	~King();
	virtual bool checkMove(int new_x, int new_y, const std::vector<std::vector<Tools*>>& _board);//check if the nove is valid
};
