#pragma once
#include <stdio.h>
#include <iostream>
#include <string>
#include <vector>
#include <iostream>
#include "Tools.h"
#include "Bishop.h"
#include "King.h"
#include "Knight.h"
#include "Pawn.h"
#include "Queen.h"
#include "Rook.h"

#define SIZE 8

/*
the class of the board of the game
*/

class BoardGame
{
protected:
	std::string _str_board;
	std::vector<std::vector<Tools*>> _board;
	int _size;

public:
	//c'tor
	BoardGame();
	//d'tor
	~BoardGame();
	//functions
	void printBoard();//print board and the str
	std::vector<std::vector<Tools*>>& getBoard();//get refernc to the board
	void update_str_board(int who_play);//updte the board
};
