#pragma once


#include <iostream>
#include <string>
#include <vector>

#define WHITE 0
#define BLACK 1
#define SIZE 8

/*
the class of all the tools in the game 
*/

class Tools
{
protected:
	int _color;
	char _type;
	int _x_place;
	int _y_place;

public:
	//c'tor
	Tools(const char type, const int x, const int y, const int color);
	//d'tor
	~Tools();
	//setters
	void setType(char type);
	void setX(int x);
	void setColor(int color);
	void setY(int y);
	//getters
	char getType();
	int getX();
	int getColor();
	int getY();
	//functions
	virtual bool checkMove(int new_x, int new_y,const std::vector<std::vector<Tools*>>& _board) = 0;
	void move(int new_x, int new_y, std::vector<std::vector<Tools*>>& _board);
	void eat(int new_x, int new_y, std::vector<std::vector<Tools*>>& _board);
	bool isPlaceTaken(int x, int y, std::vector<std::vector<Tools*>>& _board);
	bool is_chess(const std::vector<std::vector<Tools*>>& _board);
	bool can_eat(int new_x, int new_y, const std::vector<std::vector<Tools*>>& _board);
	bool moveCausesChess(int new_x, int new_y, std::vector<std::vector<Tools*>>& _board);

};