 #include "Manager.h"

/*
the c'tor
*/
Manager::Manager()
{
	this->boardGame = new BoardGame();
	this->players = new Players();
	this->_x_tools = 0;
	this->_y_tools = 0;
	this->_x_where_to_go = 0;
	this->_y_where_to_go = 0;
}

/*
the d'tor
*/
Manager::~Manager()
{
	delete this->boardGame;
	delete this->players;
}

/*
the function geyt strung og 4 letters 
then chancg tham to int
the set was if is exsust proplen 5 
input: string
output:if problen number 5 exsist
*/
bool Manager::checkAndSetDetails(const std::string moves)
{

	/*if (moves.size() != 4 && std::isalpha(moves[0]) || !(std::isalpha(moves[2]) && std::isdigit(moves[1]) && std::isdigit(moves[3])))
	{
		return false;
	}*/
	this->_x_tools = 10 - (moves[1] - '1') - 3;// 'e' -> 5; 'e' == 0x66 ==> (- 0x61); 0x61??
	this->_y_tools = moves[0] - 'a';
	this->_x_where_to_go = 10 - (moves[3] - '1') - 3;
	this->_y_where_to_go = moves[2] - 'a';
	std::cout << _x_tools << " " << _x_where_to_go << " " << std::endl;

	if ( this->_x_where_to_go < 0 || this->_x_where_to_go > 8 || this->_y_where_to_go < 0 || this->_y_where_to_go > 8 || this->_y_tools < 0 || this->_y_tools > 8 || this->_x_tools < 0 || this->_x_tools > 8)
	{
		return false;
	}
	return true;
}


/*
we check if the tools still exists
we do this to check if some one won
input: char of the tool
output:if the tools exsist
*/
bool Manager::isToolsExist(char ToolsToChack)
{
	
	for(int i = 0; i < SIZE ; i++)
	{
		for (int j = 0; j < SIZE; j++)
		{
			if (this->boardGame->getBoard()[i][j] != nullptr && this->boardGame->getBoard()[i][j]->getType() == ToolsToChack)
			{
				return true;
			}
		}
	}
	return false; 
}

/*
the menger of the game
input: string of letters
output: num between 0 to 8 
*/
char Manager::gameConsole(const std::string Places)
{
	int whoPlay = 0; 

	if(isToolsExist('k') && isToolsExist('K'))//// check if someone win
	{
		boardGame->printBoard(); //print the board 
		if (this->players->is_black())// set how play 
		{
			whoPlay = BLACK;
		}
		else if (this->players->get_white())
		{
			whoPlay = WHITE;
		}
		std::cout << "its " << whoPlay << " turn" << std::endl;

		if(checkAndSetDetails(Places)) // check if problem mun 5 exsist
		{
			
			if ((this->_x_tools == this->_x_where_to_go) && (this->_y_tools == this->_y_where_to_go))// check if problem mun 7 exsist
			{
				return '7';
			}

			if (this->boardGame->getBoard()[this->_x_where_to_go][this->_y_where_to_go] != nullptr && this->boardGame->getBoard()[this->_x_where_to_go][this->_y_where_to_go]->getColor() == whoPlay)// check if pronlem mun 3 exsist
			{
				return '3';
			}

			if (this->boardGame->getBoard()[this->_x_tools][this->_y_tools] != nullptr && this->boardGame->getBoard()[this->_x_tools][this->_y_tools]->getColor() == whoPlay)//check if problen num 2 exsist
			{
				if (this->boardGame->getBoard()[this->_x_tools][this->_y_tools]->checkMove(this->_x_where_to_go, this->_y_where_to_go, this->boardGame->getBoard()))
				{
					if (this->boardGame->getBoard()[this->_x_tools][this->_y_tools]->moveCausesChess(this->_x_where_to_go, this->_y_where_to_go, this->boardGame->getBoard()))
					{
						this->boardGame->getBoard()[this->_x_tools][this->_y_tools]->move(this->_x_where_to_go, this->_y_where_to_go, this->boardGame->getBoard());

						if (this->boardGame->getBoard()[this->_x_where_to_go][this->_y_where_to_go]->is_chess(this->boardGame->getBoard()))
						{
							std::cout << whoPlay << "can do chess" << std::endl;
							this->players->set_players();
							return '1';
						}

						boardGame->printBoard();
						this->players->set_players();
						this->boardGame->update_str_board(whoPlay);
					}
					else
					{
						return '4';
					}
				}
				else
				{
					return '6';
				}
			}
			else
			{
				return '2';
			}
		}
		else
		{
			return '5';
		}
		return '0';
	}
	else
	{
		return '8';
	}

}