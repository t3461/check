#pragma once
#include <iostream>
#include <vector>
#include <string>
#include <cmath>
#include "Tools.h"
#include "BoardGame.h"
#include "Players.h"

/*
the class of the manager of the game
*/

class Manager
{
private:
	BoardGame* boardGame;
	Players* players;
	int _x_tools; 
	int _y_tools; 
	int _x_where_to_go; 
	int _y_where_to_go;

public:
	//c'tor
	Manager();
	//d'tor
	~Manager();
	//functions
	bool isToolsExist(char ToolsToChack);
	char gameConsole(const std::string Places);
	bool checkAndSetDetails(const std::string moves); //put in the private the fields
};
