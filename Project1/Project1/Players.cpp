#include "Players.h"

/*
* constructor
* define the value that player white can 
* play and player black cant
*/
Players::Players()
{
	this->_whitePlayer = PLAY; 
	this->_blackPlayer = NOT_PLAY; 
}

// disructor
Players::~Players()
{
}

/*
return if white player can play or not 
input: none
output: bool  if white player can play or not 
*/
bool Players::get_white() const
{
	return this->_whitePlayer;
}

/*
return if black player can play or not
input: none
output: bool if black player can play or not
*/
bool Players::is_black() const
{
	return this->_blackPlayer;
}

/*
set the white and black 
replace the turn between black player and 
the white players
input: none
output: none
*/
void Players::set_players()
{
	this->_whitePlayer = !(this->_whitePlayer); 
	this->_blackPlayer = !(this->_blackPlayer);
}
