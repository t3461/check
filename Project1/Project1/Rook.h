#pragma once
#include <iostream>
#include "Tools.h"
#include "BoardGame.h"

/*
* the rook class that heir from tools
*/

class Rook : public Tools
{
public:
	//c'tor
	Rook(const char type, const int x, const int y, const int color);
	//d'tor
	~Rook();
	virtual bool checkMove(int new_x, int new_y, const std::vector<std::vector<Tools*>>& _board);//check if the nove is valid
};
