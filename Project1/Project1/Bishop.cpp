#include "Bishop.h"

/*
the c'tor
*/
Bishop::Bishop(const char type, const int x, const int y, const int color): Tools(type, x, y, color)
{
}

/*
the d'tor
*/
Bishop::~Bishop()
{
}

/*
input: the new x,y of the new place that we want to check if we can move to there and the board
output: true or false if we can move the bishop
the function check if we can move the bishop to a new place
*/
bool Bishop::checkMove(int new_x, int new_y, const std::vector<std::vector<Tools*>>& _board)
{
	if ((std::abs(this->_x_place - new_x)) == (std::abs(this->_y_place - new_y)))
	{
		int minX = std::min(this->_x_place, new_x);
		int YofMinX = 0;

		if (minX == this->_x_place)
		{
			YofMinX = this->_y_place;
		}
		else
		{
			YofMinX = new_y;
		}

		if (YofMinX != std::min(new_y, this->_y_place))
		{
			int j = std::max(new_y, this->_y_place);
			for (int i = minX; i <= std::max(this->_x_place, new_x); i++, j--)
			{
				if (_board[i][j] != nullptr)
				{
					if (_board[this->_x_place][this->_y_place] != _board[i][j] && _board[new_x][new_y] != _board[i][j])
					{
						return false;
					}
				}
			}
		}
		else
		{
			int j = YofMinX;
			for (int i = minX; i <= std::max(this->_x_place, new_x); i++, j++)
			{
				if (_board[i][j] != nullptr)
				{
					if (_board[this->_x_place][this->_y_place] != _board[i][j] && _board[new_x][new_y] != _board[i][j])
					{
						return false;
					}
				}
			}
		}
		if (_board[new_x][new_y] != nullptr)
		{
			return this->can_eat(new_x, new_y, _board);
		}
		return true;
	}
	return false; 	
}