#pragma once
#include <iostream>
#include "Tools.h"
#include <cmath>

/*
* the knight class that heir from tools
*/


class Knight : public Tools {
public:
	//c'tor
	Knight(const char type, const int x, const int y, const int color);
	//d'tor
	~Knight();
	virtual bool checkMove(int new_x, int new_y, const std::vector<std::vector<Tools*>>& _board);//check if the nove is valid
};
